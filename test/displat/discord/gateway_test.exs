defmodule Gateway do
  use ExUnit.Case, async: true

  alias LoadBalancer.Discord.ConnManager
  alias LoadBalancer.Discord.Gateway, as: Discord

  @token "tokengobrrr"
  @os "memeware"
  @device "displat"
  @session "bytheemperor"

  test "heartbeat missed" do
    state = %{
      hb_pending: true
    }

    assert catch_exit(Discord.heartbeat(state)) == :cycle_self
  end

  test "heartbeat" do
    state = %{
      hb_interval: 47,
      seq: 47
    }

    expected = {:send, %{"op" => 1, "d" => 47}}

    state = Discord.heartbeat(state)

    assert Map.has_key?(state, :hb_pending) and state[:hb_pending]

    assert_received ^expected

    assert_receive :heartbeat, 50
  end

  test "identify" do
    state = %{
      token: @token,
      intents: 47,
      os: @os,
      shard: 0,
      shards: 1
    }

    expected =
      {:send,
       %{
         "op" => 2,
         "d" => %{
           "token" => @token,
           "intents" => 47,
           "shard" => [0, 1],
           "properties" => %{
             "$os" => @os,
             "$browser" => @device,
             "$device" => @device
           }
         }
       }}

    Discord.identify(state)

    assert_received ^expected
  end

  test "identify w/ nil_intents + nil_shards" do
    state = %{
      token: @token,
      os: @os
    }

    expected =
      {:send,
       %{
         "op" => 2,
         "d" => %{
           "token" => @token,
           "intents" => 0,
           "properties" => %{
             "$os" => @os,
             "$browser" => @device,
             "$device" => @device
           }
         }
       }}

    Discord.identify(state)

    assert_received ^expected
  end

  test "resume" do
    ConnManager.set_seq(0, 47)

    state = %{
      shard: 0,
      token: @token,
      seq: 47
    }

    expected =
      {:send,
       %{
         "op" => 6,
         "d" => %{
           "token" => @token,
           "session_id" => @session,
           "seq" => 47
         }
       }}

    Discord.resume(@session, state)

    assert_received ^expected
  end

  ####
  # Packet Handler tests
  ####

  test "recv opcode 0" do
    ref = make_ref()

    packet = %{
      "op" => 0,
      "d" => ref,
      "t" => "TEST",
      "s" => 47
    }

    expected = {:dispatch, "TEST", ref}

    state = Discord.handle_packet(packet, %{})

    assert state[:seq] == 47

    assert_received ^expected
  end

  test "recv opcode 1" do
    state = %{
      seq: 47
    }

    packet = %{
      "op" => 1
    }

    expected = {:send, %{"op" => 11, "d" => 47}}

    Discord.handle_packet(packet, state)

    assert_received ^expected
  end

  test "recv opcode 7" do
    # This is necessary because the test may be ran after "w/ session"
    ConnManager.set_session(0, nil)

    assert catch_exit(Discord.handle_packet(%{"op" => 7}, %{})) == :cycle_self
    assert ConnManager.get_resume(0) == nil
  end

  test "recv opcode 7 w/ session" do
    ConnManager.set_session(0, @session)

    assert catch_exit(Discord.handle_packet(%{"op" => 7}, %{})) == :cycle_self
    assert ConnManager.get_resume(0) == @session
  end

  test "recv opcode 9" do
    ConnManager.set_session(0, @session)

    assert catch_exit(Discord.handle_packet(%{"op" => 9, "d" => true}, %{})) == :cycle_self
    assert ConnManager.get_resume(0) == @session
  end

  test "recv opcode 9 w/o resume" do
    ConnManager.set_session(0, @session)

    assert catch_exit(Discord.handle_packet(%{"op" => 9, "d" => false}, %{})) == :cycle_self
    assert ConnManager.get_resume(0) == nil
  end

  test "recv opcode 10 w/ resume" do
    ConnManager.set_session(0, @session)
    catch_exit(ConnManager.cycle_self(0))

    packet = %{
      "op" => 10,
      "d" => %{
        # long enough to be omited from test
        "heartbeat_interval" => 600_000
      }
    }

    state = Discord.handle_packet(packet, %{})

    assert state[:hb_interval] == 600_000
    assert_received {:resume, @session}
    assert_received :heartbeat
  end

  test "recv opcode 10 w/ identify" do
    ConnManager.set_session(0, @session)
    catch_exit(ConnManager.cycle_self(0, false))

    packet = %{
      "op" => 10,
      "d" => %{
        # long enough to be omited from test
        "heartbeat_interval" => 600_000
      }
    }

    state = Discord.handle_packet(packet, %{})

    assert state[:hb_interval] == 600_000
    assert_received :identify
    assert_received :heartbeat
  end

  test "recv opcode 11" do
    state = Discord.handle_packet(%{"op" => 11}, %{})

    refute state[:hb_pending]
  end
end
