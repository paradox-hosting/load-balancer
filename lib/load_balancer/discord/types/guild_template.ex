defmodule LoadBalancer.Discord.Types.GuildTemplate do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/template)"

  alias LoadBalancer.Discord.Types.{Guild, User}

  defstruct [
    :code,
    :name,
    :description,
    :usage_count,
    :creator_id,
    :creator,
    :created_at,
    :updated_at,
    :source_guild_id,
    :serialized_source_guild,
    :is_dirty
  ]

  @typedoc "the template code (unique ID)"
  @type code :: String.t()

  @typedoc "template name"
  @type name :: String.t()

  @typedoc "the description for the template"
  @type description :: String.t() | nil

  @typedoc "number of times this template has been used"
  @type usage_count :: integer

  @typedoc "the ID of the user who created the template"
  @type creator_id :: User.id()

  @typedoc "the user who created the template"
  @type creator :: User.t()

  @typedoc "ISO8601 timestamp when this template was created"
  @type created_at :: String.t()

  @typedoc "ISO8601 timestamp when this template was last synced to the source guild"
  @type updated_at :: String.t()

  @typedoc "the ID of the guild this template is based on"
  @type source_guild_id :: Guild.id()

  @typedoc "the guild snapshot this template contains (a partial guild object)"
  @type serialized_source_guild :: Guild.t()

  @typedoc "whether the template has unsynced changes"
  @type is_dirty :: boolean | nil

  @type t :: %__MODULE__{
          code: code,
          name: name,
          description: description,
          usage_count: usage_count,
          creator_id: creator_id,
          creator: creator,
          created_at: created_at,
          updated_at: updated_at,
          source_guild_id: source_guild_id,
          serialized_source_guild: serialized_source_guild,
          is_dirty: is_dirty
        }
end
