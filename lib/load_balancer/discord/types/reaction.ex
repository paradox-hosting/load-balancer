defmodule LoadBalancer.Discord.Types.Reaction do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/channel#reaction-object)"

  defstruct [
    :count,
    :me,
    :emoji
  ]

  @typedoc "times this emoji has been used to react"
  @type count :: integer

  @typedoc "whether the current user reacted using this emoji"
  @type me :: boolean

  @typedoc "a *partial* Emoji.t()"
  @type emoji :: map()

  @type t :: %__MODULE__{
          count: count,
          me: me,
          emoji: emoji
        }
end
