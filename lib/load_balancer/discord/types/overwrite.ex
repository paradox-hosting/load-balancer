defmodule LoadBalancer.Discord.Types.Overwrite do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/channel#overwrite-object)"

  alias LoadBalancer.Discord.Types.Snowflake

  defstruct [
    :id,
    :type,
    :allow,
    :deny
  ]

  @typedoc "Role or User id"
  @type id :: Snowflake.t()

  @typedoc "Either 'role' or 'member'"
  @type type :: String.t()

  @typedoc "Permission bit set"
  @type allow :: integer

  @typedoc "Permission bit set"
  @type deny :: integer

  @type t :: %__MODULE__{
          id: id,
          type: type,
          allow: allow,
          deny: deny
        }
end
