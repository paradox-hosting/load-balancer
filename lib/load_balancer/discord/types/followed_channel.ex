defmodule LoadBalancer.Discord.Types.FollowedChannel do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/channel#followed-channel-object)"

  alias LoadBalancer.Discord.Types.Channel
  alias LoadBalancer.Discord.Types.Webhook

  defstruct [
    :channel_id,
    :webhook_id
  ]

  @typedoc "source channel id"
  @type channel_id :: Channel.id()

  @typedoc "created target webhook id"
  @type webhook_id :: Webhook.id()

  @type t :: %__MODULE__{
          channel_id: channel_id,
          webhook_id: webhook_id
        }
end
