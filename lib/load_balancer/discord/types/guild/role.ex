defmodule LoadBalancer.Discord.Types.Guild.Role do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/permissions#role-object)"

  alias LoadBalancer.Discord.Types.Guild.Role.Tags
  alias LoadBalancer.Discord.Types.Snowflake

  defstruct [
    :id,
    :name,
    :color,
    :hoist,
    :position,
    :permissions,
    :managed,
    :mentionable,
    :tags
  ]

  @typedoc "The id of the role"
  @type id :: Snowflake.t()

  @typedoc "The name of the role"
  @type name :: String.t()

  @typedoc "The hexadecimal color code"
  @type color :: integer

  @typedoc "Whether the role is pinned in the user listing"
  @type hoist :: boolean

  @typedoc "The position of the role"
  @type position :: integer

  @typedoc "The permission bit set"
  @type permissions :: integer

  @typedoc "Whether the role is managed by an integration"
  @type managed :: boolean

  @typedoc "Whether the role is mentionable"
  @type mentionable :: boolean

  @typedoc "The tags this role has"
  @type tags :: Tags.t()

  @type t :: %__MODULE__{
          id: id,
          name: name,
          color: color,
          hoist: hoist,
          position: position,
          permissions: permissions,
          managed: managed,
          mentionable: mentionable,
          tags: tags
        }
end
