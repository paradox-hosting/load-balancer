defmodule LoadBalancer.Discord.Types.Guild.Ban do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/guild#ban-object)"

  alias LoadBalancer.Discord.Types.User

  defstruct [
    :reason,
    :user
  ]

  @typedoc "the reason for the ban"
  @type reason :: String.t() | nil

  @typedoc "the banned user"
  @type user :: User.t()

  @type t :: %__MODULE__{
          reason: reason,
          user: user
        }
end
