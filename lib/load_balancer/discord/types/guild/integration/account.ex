defmodule LoadBalancer.Discord.Types.Guild.Integration.Account do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/guild#integration-account-object)"

  alias LoadBalancer.Discord.Types.Snowflake

  defstruct [
    :id,
    :name
  ]

  @typedoc "id of the account"
  @type id :: Snowflake.t()

  @typedoc "name of the account"
  @type name :: String.t()

  @type t :: %__MODULE__{
          id: id,
          name: name
        }
end
