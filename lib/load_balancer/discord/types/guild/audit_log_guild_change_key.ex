defmodule LoadBalancer.Discord.Types.Guild.AuditLogGuildChangeKey do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/audit-log#audit-log-change-object-audit-log-change-key)"

  alias LoadBalancer.Discord.Types.{Channel, Snowflake, User}

  defstruct [
    :id,
    :type,
    :name,
    :icon_hash,
    :splash_hash,
    :owner_id,
    :region,
    :afk_channel_id,
    :afk_timeout,
    :mfa_level,
    :verification_level,
    :explicit_content_filter,
    :default_message_notifications,
    :vanity_url_code,
    :add,
    :remove,
    :prune_delete_days,
    :widget_enabled,
    :widget_channel_id,
    :system_channel_id
  ]

  @typedoc "the id of the changed entity - sometimes used in conjunction with other keys"
  @type id :: Snowflake.t()

  @typedoc "type of entity created"
  @type type :: Channel.type() | String.t()

  @typedoc "name changed"
  @type name :: String.t()

  @typedoc "icon changed"
  @type icon_hash :: String.t()

  @typedoc "invite splash page artwork changed"
  @type splash_hash :: String.t()

  @typedoc "owner changed"
  @type owner_id :: User.id()

  @typedoc "region changed"
  @type region :: String.t()

  @typedoc "afk channel changed"
  @type afk_channel_id :: Channel.id()

  @typedoc "afk timeout duration changed"
  @type afk_timeout :: integer()

  @typedoc "two-factor auth requirement changed"
  @type mfa_level :: integer()

  @typedoc "required verification level changed"
  @type verification_level :: integer()

  @typedoc "change in whose messages are scanned and deleted for explicit content in the server"
  @type explicit_content_filter :: integer()

  @typedoc "default message notification level changed"
  @type default_message_notifications :: integer()

  @typedoc "guild invite vanity url changed"
  @type vanity_url_code :: String.t()

  @typedoc "new role(s) added. These are *partial* roles, as in `Role.t()`"
  @type add :: [map()]

  @typedoc "role(s) removed. These are *partial* roles, as in `Role.t()`"
  @type remove :: [map()]

  @typedoc "change in number of days after which inactive and role-unassigned members are kicked"
  @type prune_delete_days :: integer()

  @typedoc "server widget enabled/disable"
  @type widget_enabled :: boolean()

  @typedoc "channel id of the server widget changed"
  @type widget_channel_id :: Channel.id()

  @typedoc "id of the system channel changed"
  @type system_channel_id :: Channel.id()

  @type t :: %__MODULE__{
          id: id(),
          type: type(),
          name: name(),
          icon_hash: icon_hash(),
          splash_hash: splash_hash(),
          owner_id: owner_id(),
          region: region(),
          afk_channel_id: afk_channel_id(),
          afk_timeout: afk_timeout(),
          mfa_level: mfa_level(),
          verification_level: verification_level(),
          explicit_content_filter: explicit_content_filter(),
          default_message_notifications: default_message_notifications(),
          vanity_url_code: vanity_url_code(),
          add: add(),
          remove: remove(),
          prune_delete_days: prune_delete_days(),
          widget_enabled: widget_enabled(),
          widget_channel_id: widget_channel_id(),
          system_channel_id: system_channel_id()
        }
end
