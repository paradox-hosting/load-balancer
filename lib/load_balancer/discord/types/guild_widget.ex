defmodule LoadBalancer.Discord.Types.GuildWidget do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/guild#guild-widget-object-guild-widget-structure)"

  alias LoadBalancer.Discord.Types.Channel

  defstruct [
    :enabled,
    :channel_id
  ]

  @typedoc "whether the widget is enabled"
  @type enabled :: boolean()

  @typedoc "the widget channel id"
  @type channel_id :: Channel.id() | nil

  @type t :: %__MODULE__{
          enabled: enabled,
          channel_id: channel_id
        }
end
