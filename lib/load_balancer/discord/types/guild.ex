defmodule LoadBalancer.Discord.Types.Guild do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/guild#guild-object)"

  alias LoadBalancer.Discord.Types.{Channel, Emoji, User}
  alias LoadBalancer.Discord.Types.Guild.{Member, Role}
  alias LoadBalancer.Discord.Types.Snowflake

  defstruct [
    :id,
    :name,
    :icon,
    :icon_hash,
    :splash,
    :discovery_splash,
    :owner,
    :owner_id,
    :permissions,
    :region,
    :afk_channel_id,
    :afk_timeout,
    :widget_enabled,
    :widget_channel_id,
    :verification_level,
    :default_message_notifications,
    :explicit_content_filter,
    :roles,
    :emojis,
    :features,
    :mfa_level,
    :application_id,
    :system_channel_id,
    :system_channel_flags,
    :rules_channel_id,
    :joined_at,
    :large,
    :unavailable,
    :member_count,
    :voice_states,
    :members,
    :channels,
    :presences,
    :max_presences,
    :max_members,
    :vanity_url_code,
    :description,
    :banner,
    :premium_tier,
    :premium_subscription_count,
    :preferred_locale,
    :public_updates_channel_id,
    :max_video_channel_users,
    :approximate_member_count,
    :approximate_presence_count
  ]

  @typedoc "The guild's id"
  @type id :: Snowflake.t()

  @typedoc "The name of the guild."
  @type name :: String.t()

  @typedoc "The hash of the guild's icon"
  @type icon :: String.t() | nil

  @typedoc "The guild's icon hash (returned when part of a Template)"
  @type icon_hash :: String.t() | nil

  @typedoc "The hash of the guild's splash"
  @type splash :: String.t() | nil

  @typedoc "discovery splash hash; only present for guilds with the \"DISCOVERABLE\" feature"
  @type discovery_splash :: String.t() | nil

  @typedoc "true if the user is the owner of the guild"
  @type owner :: boolean

  @typedoc "The id of the guild owner"
  @type owner_id :: User.id()

  @typedoc "total permissions for the user in the guild (excludes overrides)"
  @type permissions :: String.t()

  @typedoc "The id of the voice region"
  @type region :: String.t()

  @typedoc "The id of the guild's afk channel"
  @type afk_channel_id :: Channel.id() | nil

  @typedoc "The time someone must be afk before being moved"
  @type afk_timeout :: integer

  @typedoc "true if the server widget is enabled"
  @type widget_enabled :: boolean | nil

  @typedoc "the channel id that the widget will generate an invite to, or null if set to no invite"
  @type widget_channel_id :: Channel.id() | nil

  @typedoc "The level of verification. See [the documentation](https://discord.com/developers/docs/resources/guild#guild-object-verification-level)"
  @type verification_level :: integer

  @typedoc """
  Default message notifications level. See [the documentation](https://discord.com/developers/docs/resources/guild#guild-object-default-message-notification-level)
  """
  @type default_message_notifications :: integer

  @typedoc """
  Explicit content filter level. See [the documentation](https://discord.com/developers/docs/resources/guild#guild-object-explicit-content-filter-level)
  """
  @type explicit_content_filter :: integer

  @typedoc "List of roles"
  @type roles :: [Role.t()]

  @typedoc "List of emojis"
  @type emojis :: [Emoji.t()]

  @typedoc "List of guild features"
  @type features :: [String.t()]

  @typedoc "Required MFA level of the guild"
  @type mfa_level :: integer

  @typedoc """
  Application id of the guild creator if it is bot created.
  """
  @type application_id :: Snowflake.t() | nil

  @typedoc """
  The id of the channel to which system messages are sent.
  """
  @type system_channel_id :: Snowflake.t() | nil

  @typedoc "See [the documentation](https://discord.com/developers/docs/resources/guild#guild-object-system-channel-flags)"
  @type system_channel_flags :: integer()

  @typedoc "the id of the channel where Community guilds can display rules and/or guidelines"
  @type rules_channel_id :: Channel.id() | nil

  @typedoc "Date the bot user joined the guild (ISO8601 timestamp)"
  @type joined_at :: String.t() | nil

  @typedoc "Whether the guild is considered 'large'"
  @type large :: boolean | nil

  @typedoc "Whether the guild is avaliable"
  @type unavailable :: boolean | nil

  @typedoc "Total number of members in the guild"
  @type member_count :: integer | nil

  @typedoc "List of voice states as maps"
  @type voice_states :: list(map) | nil

  @typedoc "List of members"
  @type members :: [Member.t()] | nil

  @typedoc "List of channels"
  @type channels :: [Channel.t()] | nil

  @typedoc "presences of the members in the guild, will only include non-offline members if the size is greater than large threshold"
  @type presences :: [TODO]

  @typedoc "the maximum number of presences for the guild (the default value, currently 25000, is in effect when null is returned)"
  @type max_presences :: integer | nil

  @typedoc "the maximum number of members for the guild"
  @type max_members :: integer | nil

  @typedoc "the vanity url code for the guild"
  @type vanity_url_code :: String.t() | nil

  @typedoc "the description for the guild, if the guild is discoverable"
  @type description :: String.t() | nil

  @typedoc "banner hash"
  @type banner :: String.t() | nil

  @typedoc "premium tier (Server Boost level)"
  @type premium_tier :: integer

  @typedoc "the number of boosts this guild currently has"
  @type premium_subscription_count :: integer | nil

  @typedoc "the preferred locale of a Community guild; used in server discovery and notices from Discord; defaults to \"en-US\""
  @type preferred_locale :: String.t()

  @typedoc "the id of the channel where admins and moderators of Community guilds receive notices from Discord"
  @type public_updates_channel_id :: Channel.id() | nil

  @typedoc "the maximum amount of users in a video channel"
  @type max_video_channel_users :: integer() | nil

  @typedoc "approximate number of members in this guild, returned from the GET /guild/<id> endpoint when with_counts is true"
  @type approximate_member_count :: integer | nil

  @typedoc "approximate number of non-offline members in this guild, returned from the GET /guild/<id> endpoint when with_counts is true"
  @type approximate_presence_count :: integer | nil

  @type t :: %__MODULE__{
          id: id(),
          name: name(),
          icon: icon(),
          icon_hash: icon_hash(),
          splash: splash(),
          discovery_splash: discovery_splash(),
          owner: owner(),
          owner_id: owner_id(),
          permissions: permissions(),
          region: region(),
          afk_channel_id: afk_channel_id(),
          afk_timeout: afk_timeout(),
          widget_enabled: widget_enabled(),
          widget_channel_id: widget_channel_id(),
          verification_level: verification_level(),
          default_message_notifications: default_message_notifications(),
          explicit_content_filter: explicit_content_filter(),
          roles: roles(),
          emojis: emojis(),
          features: features(),
          mfa_level: mfa_level(),
          application_id: application_id(),
          system_channel_id: system_channel_id(),
          system_channel_flags: system_channel_flags(),
          rules_channel_id: rules_channel_id(),
          joined_at: joined_at(),
          large: large(),
          unavailable: unavailable(),
          member_count: member_count(),
          voice_states: voice_states(),
          members: members(),
          channels: channels(),
          presences: presences(),
          max_presences: max_presences(),
          max_members: max_members(),
          vanity_url_code: vanity_url_code(),
          description: description(),
          banner: banner(),
          premium_tier: premium_tier(),
          premium_subscription_count: premium_subscription_count(),
          preferred_locale: preferred_locale(),
          public_updates_channel_id: public_updates_channel_id(),
          max_video_channel_users: max_video_channel_users(),
          approximate_member_count: approximate_member_count(),
          approximate_presence_count: approximate_presence_count()
        }
end
