defmodule LoadBalancer.Discord.Types.Message.Reaction do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/channel#reaction-object)"

  alias LoadBalancer.Discord.Types.Emoji

  defstruct [
    :count,
    :me,
    :emoji
  ]

  @typedoc "Times this emoji has been used to react"
  @type count :: integer

  @typedoc "Whether the current user is the one who reacted"
  @type me :: boolean

  @typedoc "Emoji information"
  @type emoji :: Emoji.t()

  @type t :: %__MODULE__{
          count: count,
          me: me,
          emoji: emoji
        }
end
