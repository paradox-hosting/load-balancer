defmodule LoadBalancer.Discord.Types.Embed.Footer do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/channel#embed-object-embed-footer-structure)"

  defstruct [
    :text,
    :icon_url,
    :proxy_icon_url
  ]

  @typedoc "Footer text"
  @type text :: String.t()

  @typedoc "URL of footer icon"
  @type icon_url :: String.t() | nil

  @typedoc "Proxied URL of footer icon"
  @type proxy_icon_url :: String.t() | nil

  @type t :: %__MODULE__{
          text: text,
          icon_url: icon_url,
          proxy_icon_url: proxy_icon_url
        }
end
