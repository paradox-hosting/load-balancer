defmodule LoadBalancer.Discord.Types.Embed.Author do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/channel#embed-object-embed-author-structure)"

  defstruct [
    :name,
    :url,
    :icon_url,
    :proxy_icon_url
  ]

  @typedoc "Name of the author"
  @type name :: String.t() | nil

  @typedoc "URL of the author"
  @type url :: String.t() | nil

  @typedoc "URL of the author icon"
  @type icon_url :: String.t() | nil

  @typedoc "Proxied URL of author icon"
  @type proxy_icon_url :: String.t() | nil

  @type t :: %__MODULE__{
          name: name,
          url: url,
          icon_url: icon_url,
          proxy_icon_url: proxy_icon_url
        }
end
