defmodule LoadBalancer.Discord.Types.Event.Activity.Timestamps do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#activity-object-activity-timestamps)"

  defstruct [
    :start,
    :end
  ]

  @typedoc "unix time (in milliseconds) of when the activity started"
  @type start_time :: integer() | nil

  @typedoc "unix time (in milliseconds) of when the activity ends"
  @type end_time :: integer() | nil

  @type t :: %__MODULE__{
          start: start_time,
          end: end_time
        }
end
