defmodule LoadBalancer.Discord.Types.Event.WebhookUpdate do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#webhooks-update)"

  alias LoadBalancer.Discord.Types.{Channel, Guild}

  defstruct [
    :guild_id,
    :channel_id
  ]

  @typedoc "id of the guild"
  @type guild_id :: Guild.id()

  @typedoc "id of the channel"
  @type channel_id :: Channel.id()

  @type t :: %__MODULE__{
          guild_id: guild_id,
          channel_id: channel_id
        }
end
