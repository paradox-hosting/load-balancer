defmodule LoadBalancer.Discord.Types.Event.Hello do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#hello)"

  defstruct [
    :heartbeat_interval
  ]

  @typedoc "the interval (in milliseconds) the client should heartbeat with"
  @type heartbeat_interval :: integer

  @type t :: %__MODULE__{
          heartbeat_interval: heartbeat_interval
        }
end
