defmodule LoadBalancer.Discord.Types.Event.ChannelPinsUpdate do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#channel-pins-update)"

  alias LoadBalancer.Discord.Types.{Channel, Guild}

  defstruct [
    :guild_id,
    :channel_id,
    :last_pin_timestamp
  ]

  @typedoc "the id of the guild"
  @type guild_id :: Guild.id() | nil

  @typedoc "the id of the channel"
  @type channel_id :: Channel.id()

  @typedoc "The ISO8601 timestamp at which the most recent pinned message was pinned"
  @type last_pin_timestamp :: String.t() | nil

  @type t :: %__MODULE__{
          guild_id: guild_id,
          channel_id: channel_id,
          last_pin_timestamp: last_pin_timestamp
        }
end
