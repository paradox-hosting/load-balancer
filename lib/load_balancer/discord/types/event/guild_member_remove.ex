defmodule LoadBalancer.Discord.Types.Event.GuildMemberRemove do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#guild-member-remove)"

  alias LoadBalancer.Discord.Types.{Guild, User}

  defstruct [
    :guild_id,
    :user
  ]

  @typedoc "the id of the guild"
  @type guild_id :: Guild.id()

  @typedoc "the user who was removed"
  @type user :: User.t()

  @type t :: %__MODULE__{
          guild_id: guild_id,
          user: user
        }
end
