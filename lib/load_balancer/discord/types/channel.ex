defmodule LoadBalancer.Discord.Types.Channel do
  @moduledoc ~S"""
  Struct representing a Discord guild channel.

  A `Channel` represents all 7 types of channels. Each
  channel has a field `:type` with any of the following values:

    * `0` - GUILD_TEXT
    * `1` - DM
    * `2` - GUILD_VOICE
    * `3` - GROUP_DM
    * `4` - GUILD_CATEGORY
    * `5` - GUILD_NEWS
    * `6` - GUILD_STORE

  More information can be found on the
  [Discord API Channel Documentation](https://discord.com/developers/docs/resources/channel#channel-object).
  """

  alias LoadBalancer.Discord.Types.{Overwrite, User}
  alias LoadBalancer.Discord.Types.Snowflake

  defstruct [
    :id,
    :type,
    :guild_id,
    :position,
    :permission_overwrites,
    :name,
    :topic,
    :nsfw,
    :last_message_id,
    :bitrate,
    :user_limit,
    :rate_limit_per_user,
    :recipients,
    :icon,
    :owner_id,
    :application_id,
    :parent_id,
    :last_pin_timestamp
  ]

  @typedoc "The channel's id"
  @type id :: Snowflake.t()

  @typedoc "The id of the channel's guild"
  @type guild_id :: Snowflake.t()

  @typedoc "The ordered position of the channel"
  @type position :: integer

  @typedoc "The list of overwrites"
  @type permission_overwrites :: [Overwrite.t()]

  @typedoc "The name of the channel"
  @type name :: String.t()

  @typedoc "Current channel topic"
  @type topic :: String.t()

  @typedoc "If the channel is nsfw"
  @type nsfw :: boolean

  @typedoc "Id of the last message sent"
  @type last_message_id :: Snowflake.t() | nil

  @typedoc "The bitrate of the voice channel"
  @type bitrate :: integer

  @typedoc "The user limit of the voice channel"
  @type user_limit :: integer

  @typedoc """
  amount of seconds a user has to wait before sending another message (0-21600);
  bots, as well as users with the permission manage_messages or manage_channel,
  are unaffected
  """
  @type rate_limit_per_user :: integer

  @typedoc "The recipients of the DM"
  @type recipients :: [User.t()]

  @typedoc "The icon hash of the channel"
  @type icon :: String.t() | nil

  @typedoc "The id of the DM creator"
  @type owner_id :: Snowflake.t()

  @typedoc "The application id of the group DM creator if it is bot-created"
  @type application_id :: Snowflake.t() | nil

  @typedoc "The id of the parent category for a channel"
  @type parent_id :: Snowflake.t() | nil

  @typedoc "When the last pinned message was pinned"
  @type last_pin_timestamp :: String.t() | nil

  @type type_guild_text :: 0
  @type type_dm :: 1
  @type type_guild_voice :: 2
  @type type_group_dm :: 3
  @type type_guild_category :: 4
  @type type_guild_news :: 5
  @type type_guild_store :: 6

  @type type ::
          type_guild_text
          | type_dm
          | type_guild_voice
          | type_group_dm
          | type_guild_category
          | type_guild_news
          | type_guild_store

  @typedoc """
  Represents a text channel in a guild.
  """
  @type t_guild_text :: %__MODULE__{
          id: id,
          type: type_guild_text,
          guild_id: guild_id,
          position: position,
          permission_overwrites: permission_overwrites,
          name: name,
          topic: topic,
          nsfw: nsfw,
          last_message_id: last_message_id,
          bitrate: nil,
          user_limit: nil,
          rate_limit_per_user: rate_limit_per_user,
          recipients: nil,
          icon: nil,
          owner_id: nil,
          application_id: nil,
          parent_id: parent_id,
          last_pin_timestamp: last_pin_timestamp
        }

  @typedoc """
  Represents a DM channel.
  """
  @type t_dm :: %__MODULE__{
          id: id,
          type: type_dm,
          guild_id: nil,
          position: nil,
          permission_overwrites: nil,
          name: nil,
          topic: nil,
          nsfw: nil,
          last_message_id: last_message_id,
          bitrate: nil,
          user_limit: nil,
          rate_limit_per_user: rate_limit_per_user,
          recipients: recipients,
          icon: nil,
          owner_id: nil,
          application_id: nil,
          parent_id: nil,
          last_pin_timestamp: nil
        }

  @typedoc """
  Represents a voice channel in a guild.
  """
  @type t_guild_voice :: %__MODULE__{
          id: id,
          type: type_guild_voice,
          guild_id: guild_id,
          position: position,
          permission_overwrites: permission_overwrites,
          name: name,
          topic: nil,
          nsfw: nsfw,
          last_message_id: nil,
          bitrate: bitrate,
          user_limit: user_limit,
          rate_limit_per_user: rate_limit_per_user,
          recipients: nil,
          icon: nil,
          owner_id: nil,
          application_id: nil,
          parent_id: parent_id,
          last_pin_timestamp: nil
        }

  @typedoc """
  Represents a group DM channel.
  """
  @type t_group_dm :: %__MODULE__{
          id: id,
          type: type_group_dm,
          guild_id: nil,
          position: nil,
          permission_overwrites: nil,
          name: name,
          topic: nil,
          nsfw: nil,
          last_message_id: last_message_id,
          bitrate: nil,
          user_limit: nil,
          rate_limit_per_user: rate_limit_per_user,
          recipients: recipients,
          icon: icon,
          owner_id: owner_id,
          application_id: application_id,
          parent_id: nil,
          last_pin_timestamp: nil
        }

  @typedoc """
  Represents a channel category in a guild.
  """
  @type t_guild_category :: %__MODULE__{
          id: id,
          type: type_guild_category,
          guild_id: guild_id,
          position: position,
          permission_overwrites: permission_overwrites,
          name: name,
          topic: nil,
          nsfw: nsfw,
          last_message_id: nil,
          bitrate: nil,
          user_limit: nil,
          recipients: nil,
          icon: nil,
          owner_id: nil,
          application_id: nil,
          parent_id: parent_id,
          last_pin_timestamp: nil
        }

  @typedoc "a channel that users can follow and crosspost into their own server"
  @type t_guild_news :: %__MODULE__{
          id: id,
          type: type_guild_news,
          guild_id: nil | guild_id,
          position: nil | position,
          permission_overwrites: nil | permission_overwrites,
          name: nil | name,
          topic: nil | topic,
          nsfw: nil | nsfw,
          last_message_id: nil | last_message_id,
          bitrate: nil,
          user_limit: nil | user_limit,
          rate_limit_per_user: nil | rate_limit_per_user,
          recipients: nil | recipients,
          icon: nil | icon,
          owner_id: nil | owner_id,
          application_id: nil | application_id,
          parent_id: parent_id,
          last_pin_timestamp: nil | last_pin_timestamp
        }

  @typedoc "a channel in which game developers can sell their game on Discord"
  @type t_guild_store :: %__MODULE__{
          id: id,
          type: type_guild_news,
          guild_id: nil | guild_id,
          position: nil | position,
          permission_overwrites: nil | permission_overwrites,
          name: nil | name,
          topic: nil | topic,
          nsfw: nil | nsfw,
          last_message_id: nil | last_message_id,
          bitrate: nil,
          user_limit: nil | user_limit,
          rate_limit_per_user: nil | rate_limit_per_user,
          recipients: nil | recipients,
          icon: nil | icon,
          owner_id: nil | owner_id,
          application_id: nil | application_id,
          parent_id: parent_id,
          last_pin_timestamp: nil | last_pin_timestamp
        }

  @typedoc "See [the documentation](https://discord.com/developers/docs/resources/channel#channel-object-channel-structure)"
  @type t ::
          t_guild_category
          | t_guild_news
          | t_guild_store
          | t_guild_text
          | t_guild_voice
end
