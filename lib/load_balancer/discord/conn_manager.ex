defmodule LoadBalancer.Discord.ConnManager do
  @moduledoc """
  Manager of all connections to the Discord Gateway
  """

  use GenServer

  require Logger
  import Logger

  @spec start_link(any) :: :ignore | {:error, any} | {:ok, pid}
  @doc """
  Starts the connection manager. `:intents` defaults to none (`0`), `:shards` defaults to `nil`,
  which enables auto sharding
  """
  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  @impl true
  def init(data) do
    data = Map.put(data, :url, "wss://gateway.discord.gg") |> Map.put(:shard_data, %{})

    shards =
      if Mix.env() != :test do
        data[:shards]
      else
        1
      end

    # Check whether we have been given a specific shards value
    # If we have not, autocalculate it.
    shard_count =
      case shards do
        nil ->
          info("Calculating shards based on Discord recommendation")
          auto_shard(data)

        n when is_integer(n) ->
          info("Setting shards to #{n} as per config")
          n
      end

    send(self(), :spawn_shard)

    {:ok,
     %{shard_data: %{}, to_spawn: shard_count, total_shards: shard_count, default_data: data}}
  end

  defp start_shard(shard, data, total_shards) do
    # Create the state object for the shard by informing the shard
    # which one in the cluster it is.
    state = Map.merge(data, %{shard: shard, shards: total_shards})

    DynamicSupervisor.start_child(
      LoadBalancer.Discord.DynamicSupervisor,
      {LoadBalancer.Discord.WS, state}
    )

    info("Spawning shard #{shard}")

    data
  end

  defp auto_shard(data) do
    # Build the new Finch client.
    client =
      Finch.build(
        :get,
        "https://discord.com/api/v8/gateway/bot",
        [
          {"Authorization", "Bot " <> data[:token]},
          {"User-Agent",
           "DiscordBot (https://gitlab.com/displat, #{Mix.Project.config()[:version]})"}
        ]
      )

    # Make the request
    {:ok, resp} = Finch.request(client, LoadBalancer.Http)

    case resp.status do
      # 200: We have a shards response
      200 ->
        # Decode the body, log and return
        shards = Poison.decode!(resp.body)["shards"]
        info("Discord recommended #{shards} shards")
        shards

      # 429: We are being ratelimited
      429 ->
        # Find the time in seconds that we have to wait
        period = Poison.decode!(resp.body)["retry_after"]

        # Warn in the console
        warn("Discord shard endpoint ratelimited, trying again in #{period} seconds.")

        # Convert the seconds float to a milliseconds integer
        Process.sleep(ceil(period * 1000))

        # Try again
        auto_shard(data)
    end
  end

  @spec set_session(integer(), binary()) :: :ok
  def set_session(shard, session) do
    GenServer.cast(__MODULE__, {:set_session, shard, session})
  end

  @spec set_seq(integer(), binary()) :: :ok
  def set_seq(shard, seq) do
    GenServer.cast(__MODULE__, {:set_seq, shard, seq})
  end

  @spec get_seq(integer()) :: binary()
  def get_seq(shard) do
    GenServer.call(__MODULE__, {:get_seq, shard})
  end

  @spec cycle_self(integer(), boolean()) :: no_return()
  def cycle_self(shard, resume \\ true) do
    GenServer.cast(__MODULE__, {:resume, shard, resume})
    exit(:cycle_self)
  end

  @spec get_resume(integer()) :: binary() | nil
  def get_resume(shard) do
    GenServer.call(__MODULE__, {:resume, shard})
  end

  # Server

  @impl true
  def handle_cast({:set_session, shard, session}, state) do
    shard = shard_sanitise(shard)
    state = put_in(state, [:shard_data, shard, :session_id], session)

    {:noreply, state}
  end

  @impl true
  def handle_cast({:set_seq, shard, seq}, state) do
    shard = shard_sanitise(shard)
    state = put_in(state, [:shard_data, shard, :seq], seq)

    {:noreply, state}
  end

  @impl true
  def handle_cast({:resume, shard, resume}, state) do
    shard = shard_sanitise(shard)
    state = put_in(state, [:shard_data, shard, :resume], resume)

    {:noreply, state}
  end

  @impl true
  def handle_call({:get_seq, shard}, _caller, state) do
    shard = shard_sanitise(shard)
    {:reply, state[:shard_data][shard][:seq], state}
  end

  @impl true
  def handle_call({:resume, shard}, _caller, state) do
    shard = shard_sanitise(shard)

    return =
      if state[:shard_data][shard][:resume] do
        state[:shard_data][shard][:session_id]
      else
        nil
      end

    {:reply, return, state}
  end

  @impl true
  def handle_info(:spawn_shard, state) do
    shard_no = state.total_shards - state.to_spawn

    new_shard_data =
      Map.put(
        state.shard_data,
        shard_no,
        start_shard(shard_no, state.default_data, state.total_shards)
      )

    state = %{state | to_spawn: state.to_spawn - 1}
    state = %{state | shard_data: new_shard_data}

    if state.to_spawn > 0 do
      Process.send_after(self(), :spawn_shard, 5000)
    end

    {:noreply, state}
  end

  defp shard_sanitise(shard) do
    case shard do
      nil -> 0
      x when is_integer(x) and x >= 0 -> x
      _ -> raise "Shard is not a natural number"
    end
  end
end
